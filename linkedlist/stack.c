#include <stdio.h>
#include <stdlib.h>

typedef struct ll_int {
  int val;
  struct ll_int *next;
} ll_int;

void print_list(ll_int *node) {
  if (!node) {
    return;
  }
  printf("value: %d\n", node->val);
  print_list(node->next);
}

void free_list(ll_int *node) {
  if (!node) {
    return;
  }
  ll_int *next = node->next; 
  free(node);
  // TAIL CALL OPTIMIZATION
  free_list(next);
}

ll_int* newnode(int value) {
  ll_int* output = (ll_int *)malloc(sizeof(ll_int));
  output->val = value;

  return output;
}

ll_int* push(int value, ll_int *stack) {
  ll_int *newfront = newnode(value);

  // glom it onto the front
  newfront->next = stack;

  // return the new front.
  return newfront;
}

int pop(ll_int **stack) {
  if ((*stack) == NULL) {
    return -1;
  }

  int out = (**stack).val;
  ll_int *freethis = (*stack);

  // change the stack location in the function that called us.
  (*stack) = (*stack)->next;

  free(freethis);
  return out;
}

int main(void) {

  ll_int *mystack = NULL;

  // create 4 nodes
  mystack = push(20, mystack);
  mystack = push(4, mystack);
  mystack = push(700, mystack);
  mystack = push(19, mystack);

  print_list(mystack);


  // free 4 nodes
  while (mystack != NULL) {
    int val = pop(&mystack);
    printf("%d\n", val);
  }

  return 0;
}
