#include <stdio.h>
#include "newton_sqrt.h"

int main(void) {
  double root = newton_sqrt(2.0);
  printf("sqrt of 2: %f\n", root);

  root = newton_sqrt(100);
  printf("sqrt of 100: %f\n", root);

  root = newton_sqrt(17);
  printf("sqrt of 17: %f\n", root);

  return 0;
}
