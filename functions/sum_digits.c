#include <stdio.h>
#include "sum_digits.h"

// we want to write a function that takes a number and interprets it as base-10,
// and returns the sum of all of its digits.
// our plan is to repeatedly grab the lowest digit, add that to a running total,
// and then (integer) divide by ten to get the next lowest digit.
// we then return the total.

int sum_digits(int number) {
  int total = 0;

  while (number > 0) {
    int digit = number % 10;
    total += digit;
    number /= 10;
  }
  return total;
}
