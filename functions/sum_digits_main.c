
#include <stdio.h>
#include "sum_digits.h"

int main(void) {

  int summed = sum_digits(50755);
  printf("should be 22: %d\n", summed);

  return 0;
}
