#include <stdio.h>
#include <stdbool.h>

bool does_x_occur_in_array(int x, int *array, int len) {
  for(int i = 0; i < len; i++) {
    if (x == array[i]) {
      return true;
    }
  }
  return false;
}

int main(void) {

  // indexing!
  int numbers[5] = {5, 7, 11, 3, 21};

  printf("thing at spot 3: %d\n", numbers[3]);

  printf("memory address of the array: %p\n", numbers);
  printf("memory address of 0th element: %p\n", &(numbers[0]));

  printf("1th element: %d\n", numbers[1]);
  printf("1th element: %d\n", *(numbers + 1));

  printf("does 11 appear? %d\n",  does_x_occur_in_array(11, numbers, 5));
  printf("does 772 appear? %d\n",  does_x_occur_in_array(772, numbers, 5));

  return 0;
}
