documents = [["pie", "tomatoes", "sadness"],
             ["pie", "pie", "pie"],
             ["tomatoes", "tomatoes", "tomatoes", "tomatoes"]]

def count_food_words(document):
    total = 0
    for word in document:
        if word in ["pie", "tomatoes"]:
            total += 1
    return total

def reduce(f, initial, items):
    current = initial
    for item in items:
        current = f(current, item)
    return current

def add(x, y):
    return x + y

mapped = [count_food_words(doc) for doc in documents]
print(mapped)

print(reduce(max, 0, mapped))
