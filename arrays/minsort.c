#include <stdio.h>
#include <limits.h>

void swap(int *a, int *b) {
  int tmp = *a;
  *a = *b;
  *b = tmp;
}

void minsort(int *arr, int len) {
  int smallest = INT_MAX;
  int smallest_index = -1;

  if (len == 0) {
    return;
  }
  // find smallest number in the array.
  for (int i = 0; i < len; i++) {
    if (arr[i] < smallest) {
      smallest = arr[i];
      smallest_index = i;
    }
  }
  // swap smallest with the thing at index 0
  swap(arr, (arr + smallest_index));

  // sort the rest of the array. RECURSIVE CALL!!!
  minsort(arr + 1, len - 1);
}


void minsort_iter(int *arr, int len) {
  int smallest;
  int smallest_index;

  for (int start = 0; start < len; start++) {
    smallest = INT_MAX;
    smallest_index = -1;

    // find smallest number in the rest of the array.
    for (int i = start; i < len; i++) {
      if (arr[i] < smallest) {
        smallest = arr[i];
        smallest_index = i;
      }
    }
    // swap smallest with the thing at index 0
    swap((arr + start), (arr + smallest_index));
  }
}

int main(void) {
  int numbers[] = {7, 25, 32, 11, 2,
                  4,  20, 13, 64, 8,
                  7, 25, 32, 11, 2,
                  4,  20, 13, 64, 8,
                  7, 25, 32, 11, 2,
                  4,  20, 13, 64, 8,
                  7, 25, 32, 11, 2,
                  4,  20, 13, 64, 8,
                  };

  minsort_iter(numbers, 40);

  for (int i = 0; i < 40; i++) {
    printf("numbers[%d] = %d\n", i, numbers[i]);
  }

  return 0;
}
