#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

void make_otp(char *buf, size_t len) {
  for (int i = 0; i < len; i++) {
    buf[i] = (random() % 0xFF);
  }
}

void bitwise_xor(char *message, char *otp, char *output, size_t len) {
  for (int i = 0; i < len; i++) {
    output[i] = message[i] ^ otp[i];
  }
}

int main(void) {
  srand(time(NULL));

  char *plaintext = "I love cake very much!";
  char *otp = strdup(plaintext);
  char *ciphertext = strdup(plaintext);
  char *output = calloc(1 + strlen(plaintext), sizeof(char));

  make_otp(otp, strlen(plaintext));


  bitwise_xor(plaintext, otp, ciphertext, strlen(plaintext));

  bitwise_xor(ciphertext, otp, output, strlen(plaintext));

  printf("OTP: %s\n", otp);
  printf("ciphertext: %s\n", ciphertext);
  printf("output: %s\n", output);

  return 0;
}
