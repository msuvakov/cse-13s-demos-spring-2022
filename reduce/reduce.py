from functools import reduce

numbers = [30, 17, -4, 12, -100, -5, -4, -107]

def count_negative(cumulative, nextnumber):
    if nextnumber < 0:
        return cumulative + 1
    else:
        return cumulative

print(reduce(count_negative, numbers, 0))
