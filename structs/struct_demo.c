#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#define MAX_NAME_LENGTH 255

typedef struct {
  int age;
  bool eats_meat;
  char name[MAX_NAME_LENGTH];
} Pet;

void print_pet(Pet *mypet) {
  printf("meet my pet %s. they are %d years old and they %s eat meat.\n",
      mypet->name,
      mypet->age,
      mypet->eats_meat ? "do" : "do not");
}

Pet* new_pet(char *name, int age, bool eats_meat) {
  Pet *output;
  output = (Pet *)calloc(1, sizeof(Pet));

  output->age = age;
  output->eats_meat = eats_meat;
  strncat(output->name, name, MAX_NAME_LENGTH);

  return output;
}

int main(void) {
  Pet spot = {5, true};
  Pet gerald;

  strncat(spot.name, "Spot", MAX_NAME_LENGTH);

  gerald.age = 10;
  gerald.eats_meat = false;
  strncat(gerald.name, "Gerald", MAX_NAME_LENGTH);

  print_pet(&spot);
  print_pet(&gerald);

  Pet *helen;
  helen = new_pet("Helen", 12, true);
  print_pet(helen);


  free(helen);

  // (void *)   -->  a pointer type that can point to any memory address
  // the NULL pointer --  points to memory address 0

  int *my_ints;
  my_ints = (int *)calloc(100, sizeof(int));
  my_ints[0] = 15;

  // give the memory back to the operating system
  free(my_ints);


  return 0;
}
