// STANDARD INPUT/OUTPUT
#include <stdio.h>

int main(void) {
  FILE *infile;

  // fopen is the function we're going to use
  infile = fopen("fanfic.txt", "r");
  char buf[1024];

  while(fgets(buf, 1024, infile)) {
    printf("I read another line!\n");
    printf("here's the line: %s\n", buf);
  }
  fclose(infile);


  infile = fopen("fanfic.txt", "r");
  int c;
  while((c = fgetc(infile)) != EOF) {
    putchar(c);
  }
  fclose(infile);



  return 0;
}
