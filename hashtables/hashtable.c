#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct FriendNode {
  char *name;   // the key
  char *fav_food;  // values
  int shoe_size; // values
  struct FriendNode *next;
} FriendNode;

#define NUM_BUCKETS 10

// Thank you Dan Bernstein.
unsigned long hash(char *str) {
  unsigned long hash = 5381;
  int c;

  while (*str != '\0') {
    c = *str;
    hash = ((hash << 5) + hash) + (unsigned char)c; /* hash * 33 + c */
    str++;
  }
  return hash;
}

FriendNode *add_friend_to_list(char *name, char *food, FriendNode *bucket) {
  FriendNode* new_friend;

  new_friend = calloc(1, sizeof(FriendNode));
  new_friend->name = strdup(name);
  new_friend->fav_food = strdup(food);
  new_friend->shoe_size = 20;
  new_friend->next = bucket;

  return new_friend;
}

void add_friend_to_hashtable(char *name, char *food,
    FriendNode **buckets, size_t num_buckets) {

  unsigned long hashvalue = hash(name);
  size_t which_bucket = hashvalue % NUM_BUCKETS;
  buckets[which_bucket] = add_friend_to_list(name, food, buckets[which_bucket]);

  return;
}

char *fav_food_for_friend(char *name, FriendNode **buckets,
    size_t num_buckets) {
  unsigned long hashvalue = hash(name);
  unsigned long which_bucket = hashvalue % NUM_BUCKETS;

  FriendNode *here = buckets[which_bucket];

  while(here) {
    if (!strcmp(here->name, name)) {
      return here->fav_food;
    }
    here = here->next;
  }
  return "";
}

int main(void) {
  // array of pointers to FriendNode because they might be null.
  FriendNode* buckets[NUM_BUCKETS] = {NULL};

  // printf("hash value for lucy: %lu\n", hashvalue);

  add_friend_to_hashtable("lucy", "pineapple", buckets, NUM_BUCKETS);
  add_friend_to_hashtable("david", "tacos", buckets, NUM_BUCKETS);
  add_friend_to_hashtable("adam", "apples", buckets, NUM_BUCKETS);
  add_friend_to_hashtable("timmy", "strawberries", buckets, NUM_BUCKETS);
  add_friend_to_hashtable("helena", "hummus", buckets, NUM_BUCKETS);

  printf("fav food for lucy: %s\n",
    fav_food_for_friend("lucy", buckets, NUM_BUCKETS));
  printf("fav food for timmy: %s\n",
    fav_food_for_friend("timmy", buckets, NUM_BUCKETS));
  printf("fav food for adam: %s\n",
    fav_food_for_friend("adam", buckets, NUM_BUCKETS));
  printf("fav food for bimmy: %s\n",
    fav_food_for_friend("bimmy", buckets, NUM_BUCKETS));

  return 0;
}
