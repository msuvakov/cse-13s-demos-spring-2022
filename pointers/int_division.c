#include <stdio.h>

int int_division(int numerator, int denominator, int *remainder) {
  int left_over = numerator;

  int quotient = 0;

  while (left_over >= denominator) {
    left_over  -= denominator;
    quotient++;
  }

  *remainder = left_over;

  return quotient;
}

int main(void) {

  int remainder = 0;
  int quotient;
  int thirteen = 0;

  quotient = int_division(5, 4, &remainder);
  printf("quotient = %d, remainder = %d\n", quotient, remainder);

  quotient = int_division(11, 4, &remainder);
  printf("quotient = %d, remainder = %d\n", quotient, remainder);

  return 0;
}
