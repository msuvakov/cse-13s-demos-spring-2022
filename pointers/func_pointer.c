#include <stdio.h>

int call_this_on_12(int (*funk)(int x) ) {
  return funk(12);
}

int add12(int x) {
  return x + 12;
}

int multiply_by_3(int x) {
  return x * 3;
}

int main(void) {
  int result = call_this_on_12(add12);
  printf("I added 12 to 12: %d\n", result);

  result = call_this_on_12(multiply_by_3);
  printf("12 times 3: %d\n", result);

  return 0;
}
