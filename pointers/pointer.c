#include <stdio.h>


void swap(int *a, int *b) {
  int a_value = *a;
  *a = *b;
  *b = a_value;
}

int main(void) {

  int sally = 2059;
  int andrew = 10266;

  printf("andrew: %d, sally: %d\n", andrew, sally);
  swap(&sally, &andrew);
  printf("andrew: %d, sally: %d\n", andrew, sally);
  
  int number = 37;

  int *location = NULL;
  location = &number;

  printf("here's the memory address: %p\n", location);
  printf("here's the memory address: %p\n", (location + 1));

  return 0;
}
