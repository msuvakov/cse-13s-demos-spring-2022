#include <sys/types.h>
#include <regex.h>
#include <stdio.h>

int main(void) {

  regex_t reg;

  regcomp(&reg, "^..b.m$", 0);

  int result = regexec(&reg, "album", 0, NULL, 0);
  printf("what was the result? 0 for success: %d\n", result);

  result = regexec(&reg, "something that doesn't contain the pattern", 0, NULL, 0);
  printf("what was the result? 0 for success: %d\n", result);

  result = regexec(&reg, "album of the year", 0, NULL, 0);
  printf("what was the result? 0 for success: %d\n", result);

  return 0;
}
