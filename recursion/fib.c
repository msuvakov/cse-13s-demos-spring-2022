#include <stdio.h>
#include <stdlib.h>

#define TABLE_SIZE 1024

long long fib(int n) {
  if (n == 0) {
    return 0;
  } else if (n == 1) {
    return 1;
  } else {
    return fib(n - 1) + fib(n - 2);
  }
}

// memoization is when we use a lookup table to avoid repeating work
// fib_table is an array
long long fib_memoize(int n, long long *fib_table) {
  if (n == 0) {
    return 0;
  } else if (n == 1) {
    return 1;
  } else {

    if (fib_table[n]) {
      // we've already computed that one!
      return fib_table[n];
    }
    long long answer = (fib_memoize(n - 1, fib_table) +
                        fib_memoize(n - 2, fib_table));
    printf("setting answer for %d: %lld\n", n, answer);
    fib_table[n] = answer;
    return answer;
  }
}

long long fib_iter(int n) {
  if (n == 0) {
    return 0;
  } else if (n == 1) {
    return 1;
  }

  long long prevprev = 0;
  long long prev = 1;
  long long cur;
  for (int i = 2; i <= n; i++) {
    cur = prevprev + prev;
    prevprev = prev;
    prev = cur;
  }
  return cur;
}

int main(void) {

  long long* fib_table = calloc(TABLE_SIZE, sizeof(long long));

  for (int i=0 ; i < 60; i++) {
    // printf("fib_iter(%d) = %lld\n", i, fib_iter(i));
  }

  for (int i=60 ; i >= 0; i--) {
    printf("fib_memoize(%d) = %lld\n", i, fib_memoize(i, fib_table));
  }
  free(fib_table);

  return 0;
}
