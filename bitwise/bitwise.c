#include <stdio.h>

void print_byte(unsigned char b) {
  for (int shift=7; shift >=0; shift--) {
    // xxxxxxxx &
    // 10000000
    // x0000000 --> 0000000x
    // we call this "masking"
    printf("%d", (b & (1 << shift)) >> shift);
  }
  printf("\n");
}

int main(void) {

  // high four bits are set
  // 11110000
  unsigned char number_a = 0xF0;

  // biggest number you can represent in the lower nibble of a byte; low four
  // bits are set.
  // 00001111
  unsigned char number_b = 0x0F;


  // 11110000 &
  // 00001111
  // 00000000
  printf("%d\n", (number_a & number_b));

  // 11110000 |
  // 00001111
  // 00000000
  printf("%d\n", (number_a | number_b));

  print_byte(number_a);
  print_byte(~number_a);
  print_byte(number_b);

  printf("%d\n", (unsigned char)~number_a);
  // printf("%d\n", number_b);

  printf("%d\n", (unsigned char)(~number_a) == number_b);

  // print_byte(0);
  //print_byte(7);

  return 0;
}
