#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct treenode {
  int val;
  struct treenode *left;
  struct treenode *right;
} treenode;

treenode *build_node(int val, treenode* left, treenode* right) {
  treenode *newnode = (treenode*)malloc(sizeof(treenode));

  newnode->val = val;
  newnode->left = left;
  newnode->right = right;
  return newnode;
}


bool tree_contains(treenode *tree, int query) {
  if (tree == NULL) {
    return false;
  }
  if (tree->val == query) {
    return true;
  } else {
    return (tree_contains(tree->left, query) ||
            tree_contains(tree->right, query));
  }
}

int main(void) {
  treenode* a = build_node(72, NULL, NULL);
  treenode* b = build_node(45, NULL, NULL);
  treenode* c = build_node(89, NULL, NULL);
  treenode* d = build_node(42, NULL, NULL);

  treenode* e = build_node(3, a, b);
  treenode* f = build_node(104, c, d);

  treenode* root = build_node(7, e, f);

  printf("does it contain 1010? %d\n", tree_contains(root, 1010));
  printf("does it contain 42? %d\n", tree_contains(root, 42));
  return 0;
}
